-------------------------------------------------------------------------------------------------------------------------
14 - 15 -  Project Deploy Nexus - Part 1,2
-------------------------------------------------------------------------------------------------------------------------
Project Introduction
Automate Nexus Deployment 
- Create a Droplet
- SSH into server and executed:
		- Download Nexus binary and unpack
		- Run Nexus application using Nexus User

Enable you to translate to Ansible Playbook
Learn common modules

Preparation 

Write 1st Play

Write 2st Play
"get_url" module
- Downloads file from HTTP,HTTPS or FTP to the remote server 
- Separate module Windows targets; "win_get_url"

"find" Module
- Return a list of files based on specific criteria
- For Windows targets: "win_find" module

Conditionals in Ansible 
- Execute task depending on some condition

"stat" Module
- Retrieve file or file system status
- Condition based on the value of a fact, a variable , or the result of a previous task

"when"
- Applies to a single task
- No double curly braces needed here!
- The when statement applies the test


0. Useful Links: 
1. - Conditionals in Ansible: https: //docs.ansible.com/ansible/latest /user_guide/playbooks_conditionals.html 
   New Modules used: 
	- -  https: //docs.ansible.com/ansible/latest /collections/ansible/builtin/get_url_module.html 
	- -  https: //docs.ansible.com/ansible/latest /collections/ansible/builtin/find_module.html 
	- -  https: //docs.ansible.com/ansible/2.10/collections/ansible/builtin/stat_module.html


---
PART-2

Write 3st Play
"group" module
- Manage presence of groups on a host
- For Windows targets: "win_group" module

"user" module
- Manage user account and user attributes
- For Windows targets: "win_user" module

"file" module
- Manage files and file properties
- For Windows targets: "win_file" module

Write 4st Play
"blockinfile" module
- Insert/update/remove a multi.line text surrounded by customizable marker lines 

"lineinfile" module
- Ensures a particular Line is in a file, or replace an existing Line using regex
- Useful when you want to change a single line in a file only
- See "replace" module to change multiple lines


Write 5st Play
"pause" module
"wait_for" module



0. New Modules used: 
	- -  https: //docs.ansible.com/ansible/latest /collections/ansible/builtin/group_module.html 
	- -  https: //docs.ansible.com/ansible/latest /collections/ansible/builtin/file_module.html 
	- -  https: //docs.ansible.com/ansible/latest /collections/ansible/builtin/blockinfile_module.html 
	- -  https: //docs.ansible.com/ansible/latest /collections/ansible/builtin/lineinfile_module.html 
	- -  https: //docs.ansible.com/ansible/latest /collections/ansible/builtin/pause_module.html 







