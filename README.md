# 15 - Configuration Management with Ansible - Automate Nexus Deployment

**Demo Project:**
Automate Nexus Deployment

**Technologies used:**
Ansible, Nexus, DigitalOcean, Java, Linux

**Project Description:**
- Create Server on DigitalOcean
- Write Ansible Playbook that creates Linux user for Nexus, configure server, installs and deploys Nexus and verifies that it is running successfully



15 - Configuration Management with Ansible
# This project was developed as part of the DevOps Bootcamp at Techword With NANA https://www.techworld-with-nana.com/devops-bootcamp





